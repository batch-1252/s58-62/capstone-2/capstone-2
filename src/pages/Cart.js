import React, {useState, useEffect, useContext} from 'react';

import {Container} from 'react-bootstrap'

import AdminView from './../components/AdminView.js';

import UserView from './../components/UserView.js';

import UserContext from './../UserContext';

export default function Cart(){

	const [carts, setCarts] = useState([]);

	const {user} = useContext(UserContext);

	const fetchData = () => {
		let token = localStorage.getItem('token')
		console.log('test')
		fetch('https://shrouded-reef-75197.herokuapp.com/api/users/view-cart',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			setCarts(result)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])


	return(
		<Container className="p-4">
			<UserView productData={carts} />
		</Container>
	)
}
