import React, {useContext, useEffect, useState} from 'react';

import UserContext from './../UserContext';

import {Link, useParams, useHistory} from 'react-router-dom';

import {Container, Card, Button} from 'react-bootstrap';

import Swal from 'sweetalert2';


export default function SpecificProduct(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0)

	const { user } = useContext(UserContext);

	const { productId } = useParams();

	let token = localStorage.getItem('token')

	let history = useHistory();

	useEffect( () => {
		fetch(`https://shrouded-reef-75197.herokuapp.com/api/products/${productId}`,
			{
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result)

			setName(result.name);
			setDescription(result.description);
			setPrice(result.price);
		})
	}, [])

	const addToCart = () => {
		fetch('https://shrouded-reef-75197.herokuapp.com/api/users/addToCart', 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					productId: productId
				})
			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result)

			if(result === true){

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Successfully Added to Cart" 
				})

				history.push('/products');
			} else {
				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Please check again" 
				})
			}
		})
	}

	return(
		<Container>
			<Card>
				<Card.Header>
					<h4>
						{name}
					</h4>
				</Card.Header>
				<Card.Body>
					<Card.Text>
						{description}
					</Card.Text>
					<h6>
						Price: Php 
						<span className="mx-2">{price}</span>
					</h6>
				</Card.Body>
				<Card.Footer>
					{
						(user.id !== null) ?
								<Button variant="primary" 
								onClick={ () => addToCart() }

								> Add to Cart</Button>
							:
								<Link className="btn btn-danger" to="/login">Login to Purchase</Link>
					}
				</Card.Footer>
			</Card>
		</Container>
	)
}