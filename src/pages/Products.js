import React, {useState, useEffect, useContext} from 'react';

import {Container} from 'react-bootstrap'

import AdminView from './../components/AdminView.js';

import UserView from './../components/UserView.js';

import UserContext from './../UserContext';

export default function Products(){

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	const fetchData = () => {
		let token = localStorage.getItem('token')
		fetch('https://shrouded-reef-75197.herokuapp.com/api/products/active',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			setProducts(result)
			console.log(result)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])

	return(
		<Container className="p-4">
			{ (user.isAdmin === true) ?
					<AdminView productData={products} fetchData={fetchData}/>
				:
					<UserView productData={products} />
			}
		</Container>
	)
}
