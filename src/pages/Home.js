import React from 'react'

import Container from 'react-bootstrap/Container'

import Highlights from './../components/Highlights';

export default function Home(){

	return(
		<Container fluid>
			<Highlights/>
		</Container>
	)
}