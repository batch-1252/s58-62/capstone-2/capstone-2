import React from 'react'

import {
	Row,
	Col,
	Card,
	Button
} from 'react-bootstrap'

export default function Highlights(){

	return(

		<Row className="px-3">
			<Col xs={12} md={4}>
				<Card>
					<Card.Body>
						<Card.Title>Promotional Sale</Card.Title>
						<Card.Text>
							Buy your favorite product for a good price.
						</Card.Text>
				    	<Button variant="primary">Enter</Button>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card>
					<Card.Body>
						<Card.Title>Product Booking</Card.Title>
						<Card.Text>
					      	Excited about a product, ask for reserve of your favorite goods. Note this promo is for bulk orders only.
						</Card.Text>
				    	<Button variant="primary">Go somewhere</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}

