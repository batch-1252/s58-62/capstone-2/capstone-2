import React, {useState, useEffect} from 'react';

import PropTypes from 'prop-types';

import {Card, Button} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Product({productProp}){
	console.log(productProp)

	const {name, description, price, _id} = productProp

	return(
		<Card className="mb-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<h5>Description</h5>
				<p>{description}</p>
				<h5>Price:</h5>
				<p>{price}</p>

		    	<Link className="btn btn-primary" to={`/products/${_id}`}>
		    		Details
		    	</Link>
			</Card.Body>
		</Card>
	)
}


Product.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}