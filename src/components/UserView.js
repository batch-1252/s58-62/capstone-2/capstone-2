import React, {useState, useEffect} from 'react';

import {Container} from 'react-bootstrap';

import Product from './Product';

export default function UserView({productData}){
	const [products, setProducts] = useState([])
	console.log(productData)
	useEffect( () => {
		const productsArr = productData.map( (product) => {

			
			if(product.isActive === true){
				return <Product key={product._id} productProp={product}/>
			} else {
				return null
			}
		})
		setProducts(productsArr)
	}, [productData])

	return(
		<Container>
			{products}
		</Container>
	)
}