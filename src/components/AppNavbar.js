import React, {Fragment, useContext} from 'react';

import { Link, NavLink, useHistory } from 'react-router-dom';

import UserContext from './../UserContext';

import {Navbar, Nav} from 'react-bootstrap';

export default function AppNavbar(){

  const {user, unsetUser} = useContext(UserContext);

  let history = useHistory();


  const logout = () => {
    unsetUser();
    history.push('/login');
  }

  console.log(user)

  let leftNav = (user.id !== null) ? 
        (user.isAdmin === true) ?
          <Fragment>
            <Nav.Link as={NavLink} to="/addProduct">Add Product</Nav.Link>
            <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
            <Nav.Link onClick={logout}>Logout</Nav.Link>
          </Fragment>
        :
          <Fragment>
            <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
            <Nav.Link onClick={logout}>Logout</Nav.Link>
          </Fragment>
        :
          (
            <Fragment>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
              </Fragment>

          )

  return (
    <Navbar bg="primary" expand="lg">
      <Navbar.Brand as={Link} to="/">The Broad Shop</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav>
          <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
        </Nav>
        <Nav>
          {leftNav}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}